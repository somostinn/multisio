��          t      �         �        �     �     �  6   �  m        �  U   �     �       O    �   ]  	             1  2   8  u   k     �  X   �     H     _        	                       
                             %sStates and Municipalities of Venezuela for WooCommerce%s plugin requires %sWooCommerce%s activated. The plugin was deactivated until you active %sWooCommerce%s Municipality Select a municipality&hellip; State States and Municipalities of Venezuela for WooCommerce This plugin allows you to choose the States and Municipalities of Venezuela in the WooCommerce address forms. Yordan Soares https://wordpress.org/plugins/states-and-municipalities-of-venezuela-for-woocommerce/ https://yordansoar.es/ required PO-Revision-Date: 2020-12-31 13:31:31+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: es
Project-Id-Version: Plugins - States and Municipalities of Venezuela for WooCommerce - Stable (latest release)
 El plugin %sEstados y Municipios de Venezuela para WooCommerce%s necesita que %sWooCommerce%s esté activo. El plugin se ha desactivado hasta que actives %sWooCommerce%s Municipio Selecciona un municipio&hellip; Estado Estados y Municipios de Venezuela para WooCommerce Este plugin te permite elegir los estados y municipios de Venezuela en los formularios de direcciones de WooCommerce. Yordan Soares https://es.wordpress.org/plugins/states-and-municipalities-of-venezuela-for-woocommerce/ https://yordansoar.es/ obligatorio 