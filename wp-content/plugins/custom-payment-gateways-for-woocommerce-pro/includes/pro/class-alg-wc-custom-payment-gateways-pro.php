<?php
/**
 * Custom Payment Gateways for WooCommerce - Pro Class
 *
 * @version 1.6.0
 * @since   1.5.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_Custom_Payment_Gateways_Pro' ) ) :

class Alg_WC_Custom_Payment_Gateways_Pro {

	/**
	 * Constructor.
	 *
	 * @version 1.5.0
	 * @since   1.5.0
	 */
	function __construct() {
		add_filter( 'alg_wc_custom_payment_gateways_values',   array( $this, 'values' ), 10, 3 );
		add_filter( 'alg_wc_custom_payment_gateways_settings', array( $this, 'settings' ), 10, 3 );
	}

	/**
	 * settings.
	 *
	 * @version 1.6.0
	 * @since   1.5.0
	 * @todo    [dev] (maybe) restrict `settings_array` to some `max` value (e.g. 100)
	 */
	function settings( $value, $type = '', $args = null ) {
		switch ( $type ) {
			case 'array':
				return array( 'step' => '1', 'min' => '1' );
			case 'array_input_fields':
			case 'array_fees':
				return array( 'step' => '1', 'min' => '0' );
			case 'array_min_amount':
				return array( 'step' => '0.00001', 'min' => '0' );
			case 'total_number':
				return '<button name="save" class="button-primary woocommerce-save-button" type="submit" value="' .
					esc_attr( __( 'Save changes', 'woocommerce' ) ) . '">' . esc_html( __( 'Save changes', 'woocommerce' ) ) . '</button>';
			default:
				return '';
		}
	}

	/**
	 * values.
	 *
	 * @version 1.6.0
	 * @since   1.5.0
	 * @todo    [dev] (maybe) better filter for `min_amount`
	 */
	function values( $value, $type, $args = null ) {
		switch ( $type ) {
			case 'total_gateways':
				return get_option( 'alg_wc_custom_payment_gateways_number', 1 );
			case 'min_amount':
				return $args->min_amount;
			case 'total_input_fields':
				return $args->get_option( 'input_fields_total', 1 );
			case 'total_fees':
				return $args->get_option( 'fees_total', 1 );
		}
		return $value;
	}

}

endif;

return new Alg_WC_Custom_Payment_Gateways_Pro();
